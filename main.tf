terraform {
   backend "http" {
  }

  required_providers {
    proxmox = {
      source  = "Telmate/proxmox"
      version = "2.9.4"
    }
  }
}

provider "proxmox" {
  pm_api_url      = "https://rpx.syscops.space:8006/api2/json"
  pm_user         = var.pm_user_rpx
  pm_password     = var.pm_pw_rpx
  pm_tls_insecure = "true"
}
provider "proxmox" {
  alias           = "soyuz"
  pm_api_url      = "https://soyuz.syscops.dev:8006/api2/json"
  pm_user         = var.pm_user_soyuz
  pm_password     = var.pm_pw_soyuz
  pm_tls_insecure = "true"
}


# resource "proxmox_vm_qemu" "template" {
#   name        = "template"
#   target_node = "hq"
#   sockets = 1
#   cores   = 2
#   cpu     = "kvm64"
#   memory  = 2048
#   agent   = 1

#   desc    = <<EOF
# created with terraform

# #szakdoga FTW
#     EOF
#   scsihw  = "virtio-scsi-pci"
#   hotplug = "disk,network,usb"
#   #boot = "cdn"
#   bootdisk = "scsi0"
#   onboot   = false


#   # or
#   clone = "ubuntu-cloud-init"
#   #preprovision = true
#   os_type = "cloud-init"

#   disk {
#     type = "scsi"

#     storage = "local-lvm"
#     size    = "32G"
#   }
#   network {
#     model    = "virtio"
#     bridge   = "vmbr1"
#     firewall = true
#   }

# }

resource "proxmox_vm_qemu" "beni-dev" {
  name        = "beni-dev"
  target_node = "hq"
  sockets     = 1
  cores       = 2
  cpu         = "kvm64"
  memory      = 2048
  agent       = 1

  desc     = <<EOF
created with terraform

#szakdoga FTW
    EOF
  scsihw   = "virtio-scsi-pci"
  hotplug  = "disk,network,usb"
  bootdisk = "scsi0"
  onboot   = false

  clone   = "ubuntu-cloud-init"
  os_type = "cloud-init"

  disk {
    type = "scsi"

    storage = "local-lvm"
    size    = "32G"
  }
  network {
    model    = "virtio"
    bridge   = "vmbr1"
    firewall = true
  }

}

resource "proxmox_vm_qemu" "beni-monitoring" {
  name        = "beni-monitoring"
  target_node = "hq"
  sockets     = 1
  cores       = 2
  cpu         = "kvm64"
  memory      = 2048
  agent       = 1

  desc     = <<EOF
created with terraform

#szakdoga FTW
    EOF
  scsihw   = "virtio-scsi-pci"
  hotplug  = "disk,network,usb"
  bootdisk = "scsi0"
  onboot   = false

  clone   = "ubuntu-cloud-init"
  os_type = "cloud-init"

  disk {
    type = "scsi"

    storage = "local-lvm"
    size    = "32G"
  }
  network {
    model    = "virtio"
    bridge   = "vmbr1"
    firewall = true
  }

}

resource "proxmox_vm_qemu" "beni-victoria" {
  name        = "beni-victoria"
  target_node = "hq"
  sockets     = 1
  cores       = 2
  cpu         = "kvm64"
  memory      = 2048
  agent       = 1

  desc     = <<EOF
created with terraform

#szakdoga FTW
    EOF
  scsihw   = "virtio-scsi-pci"
  hotplug  = "disk,network,usb"
  bootdisk = "scsi0"
  onboot   = false

  clone   = "ubuntu-cloud-init"
  os_type = "cloud-init"

  disk {
    type = "scsi"

    storage = "local-lvm"
    size    = "32G"
  }
  network {
    model    = "virtio"
    bridge   = "vmbr1"
    firewall = true
  }

}

resource "proxmox_vm_qemu" "beni_prod" {
  provider    = proxmox.soyuz
  name        = "beni-prod"
  target_node = "ns3151518"

  sockets = 1
  cores   = 2
  cpu     = "kvm64"
  memory  = 4096
  agent   = 1

  desc     = <<EOF
created with terraform

#szakdoga FTW

    EOF
  scsihw   = "virtio-scsi-pci"
  hotplug  = "disk,network,usb"
  bootdisk = "scsi0"
  onboot   = true
  clone    = "test-2"
  os_type  = "cloud-init"

  disk {
    type = "scsi"

    storage = "local"
    size    = "32G"
  }
  network {
    model    = "virtio"
    bridge   = "vmbr0"
    firewall = true
    macaddr  = "02:00:00:ac:77:68"
  }
  ipconfig0 = "ip=51.91.172.143/24,gw=51.91.172.142"
  sshkeys   = <<-EOT
            ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPO8/sAl/2EzcFwJ8qFklnM0sGfOoJ9jdmfhk/1WqO7p martin@syscops.com
            ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBbToXgnwRqznP/f/rargMq2+aD7wE5WmBfRLbL0Rd1g jozsef@sycops.com
            ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCwP8dRkvG0wii68KeQbEgueBpukdV+d7OgkLjLm2TCmJ0xGvdPs1ZJF4RxgFMIv409DmjLp9Lb0GZTsmmKy+zNPhNxz573EyKjAfd1yD0ka9WngRuMc/05Vu1UYFBISfSTRpJXFRtUu80GIRx59Go/lvpoU0UdGNnZc4rM9QXZJvauGFkZef1Gi+IXC7nxwRRT8gKcG0+3TpKapkLjkjCdSRMiCQFKVRTMFI/FZ0U9CECK3lo76RDWIDPzUqki+upQxdngfo+rM5oFT+CNVXSmz69Euv3LLiAIAVz6f9kkchAtj/93c+SVyMrEBG6mDbpwHSUWFiv7f9XdVftBtu6N beni@syscops.com
            ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQD0JMgsZlAlXl1BpfwpzloPeEvXwM/+gFWkqEbo/qynq7g7hxgw9OyLA4MMnZikKNJfMFHQjaxwMF7M8bUTYSqsg1yNu3h0mfOBdnwrRPvYgg+bEJdQ/ikYial7kBW4LzHZc3MaIHcrGWzHsq2j5rlFsFlCHBD9FMBhw8DfiMRLzNZvhDsQ4/lCVswCPnkKyI6BSRzH9v/l6oV+uzj6/soiRXqu4ei2ITxb2RLE6YzPDKYhGmoVkpqLNLKdhjCh7QyOqxHBxk5LguQ2caX7f0svhfe9Y4l9RCjPagy+JZvwJB0eAlePH19XXfuL6D4u6mrIfLagBJZd4pLQQd2xSbFkauZZkc8Qilym9wHq3hEUl52sIHZxdrGZEM5laCXaY0qZ70KHS09IHLqgOg2oU24ONy6HRtsOG1WN0dG3My5pf3rRg8lkbUiwVJ42SMUzWC/1+/QQcZi98fXT8QHQMqTlrk631Flq0fX+V7BAkF2sRE9eZz22qlfKCPJ07J8lYB0GokevuHRTrePaq8LOKe0t7sVgSe3nnpM8gHdxwokDDYwAlT/+bI5vr/pXSuvGGNHyn6bsKxuMjzVFyoYNCReT4PxvBYiDh6SH0oAjryxxNT22OAH5GGyuJabG2tBmizvZO1Mq+hZt66Qmx7C3uU9uk92HCij9bJpFyYihW5HWow== dani@syscops.com
        EOT
  ciuser    = "ubuntu"
}
